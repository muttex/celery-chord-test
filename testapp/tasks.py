import math
import gevent
from celery import Celery

app = Celery('tasks', backend='amqp://', broker='pyamqp://guest@rabbit//')
app.conf.update(
    task_serializer = 'pickle',
    result_serializer = 'pickle',
    accept_content  = ['pickle'],
)


@app.task(acks_late=True)
def longtime_task(n):
    """Emulates a long-running process"""
    print('Task for {}: started'.format(n))
    gevent.sleep(10)
    print('Task for {}: finished'.format(n))

    return n**2

@app.task(acks_late=True)
def final_task1(task_results):
    """Emulates a finishing post-processing of task results"""
    print('Final task 1 for {}'.format(task_results))
    gevent.sleep(10)

    result = sum(task_results)
    return result

@app.task(acks_late=True)
def final_task2(prev_res):
    """Emulates a post-processing that depends on final_task1"""
    print('Final task 2 for {}'.format(prev_res))
    gevent.sleep(10)

    result = math.sqrt(prev_res)
    return result

@app.task(acks_late=True)
def log_result(prev_res):
    """Emulates a post-processing that depends on final_task2"""
    print('Final task 3 for {}'.format(prev_res))
    gevent.sleep(5)

    with open('final_task3.result', 'w') as file:
        file.write(str(prev_res))
    return prev_res
