import math
import sys
from celery import chord
from tasks import longtime_task, final_task1, final_task2, log_result

def test_chord():
    """Spawn tasks with post-processing with celery.chord
    
    longtime_task is long-running exponentiation 
    final_task1 is sum of results
    final_task2 is square root of previous result
    log_result writes down result to text file
    """
    # During next stages one or both workers may be killed
    longtime_tasks = [longtime_task.si(i) for i in range(-2, 2)]
    result = chord(longtime_tasks)((final_task1.s() | final_task2.s() | log_result.s()))
    assert result.get() == math.sqrt(sum([i**2 for i in range(-2, 2)]))

if __name__ == '__main__':
    test_chord()        
